﻿using System.Linq;
using System.Net;
using System.Web.Mvc;
using TabletsUI.Models;
using TabletsUI.Models.DB;

namespace TabletsUI.Controllers
{
    public class TabletsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        //select tablets from DB
        private IQueryable<ReadyTable> SelectTabs(int? id = 0, string searchString = null)
        {
            var tablets = (from Tablet in db.Tablets
                           join Company in db.Companies on Tablet.manufacturer equals Company.id
                           join OperationSystem in db.Operation_systems on Tablet.OS equals OperationSystem.id
                           join Color in db.Colors on Tablet.color equals Color.id
                           join Definition in db.Definitions on Tablet.definition equals Definition.id
                           select new ReadyTable
                           {
                               id = Tablet.id,
                               name = Tablet.name,
                               manufacturer = Company.name,
                               OS = OperationSystem.name,
                               color = Color.color,
                               definition = Definition.definition,
                               flash = Tablet.flash,
                               price = Tablet.priceBegin,
                               date = Tablet.actualDate,
                               description = Tablet.fullDescription
                           });

            //search by id
            if (id > 0)
                tablets = tablets.Where(o => (o.id == id));

            //search by name
            if (!string.IsNullOrEmpty(searchString))
            {
                tablets = tablets.Where(o => o.name.Contains(searchString));
            }

            return tablets;
        }

        // GET: Tablets
        public ActionResult Index(string searchString)
        {
            var tablets = SelectTabs(searchString: searchString);//.ToList();
            
            return View(tablets);
        }

        // GET: Tablets/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReadyTable tablet = SelectTabs(id).FirstOrDefault();
            if (tablet == null)
            {
                return HttpNotFound();
            }
            return View(tablet);
        }
        

        // GET: Tablets/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReadyTable tablet = SelectTabs(id).FirstOrDefault();
            if (tablet == null)
            {
                return HttpNotFound();
            }
            return View(tablet);
        }

        // POST: Tablets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Tablet tablet = db.Tablets.Find(id);
            db.Tablets.Remove(tablet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
