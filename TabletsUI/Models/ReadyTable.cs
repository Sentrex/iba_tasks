﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TabletsUI.Models
{
    public class ReadyTable
    {
        [Key]
        public int id { get; set; }

        [Display(Name = "Название")]
        public string name { get; set; }

        [Display(Name = "Производитель")]
        public string manufacturer { get; set; }

        [Display(Name = "ОС")]
        public string OS { get; set; }

        [Display(Name = "Цвет")]
        public string color { get; set; }

        [Display(Name = "Разрешение экрана")]
        public string definition { get; set; }

        [Display(Name = "Флеш-память")]
        public int flash { get; set; }

        [Display(Name = "Стартовая цена")]
        public Decimal price { get; set; }

        [Display(Name = "Дата сканирования")]
        [DataType(DataType.Date)]
        public DateTime date { get; set; }

        [Display(Name = "Описание")]
        public string description { get; set; }
    }
}