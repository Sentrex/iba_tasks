﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TabletsUI.Models.DB
{
    public class Tablet
    {
        [Key]
        public int id { get; set; }

        public int manufacturer { get; set; }

        public string name { get; set; }

        public int OS { get; set; }

        public int flash { get; set; }
        
        public int color { get; set; }

        public int definition { get; set; }
        
        public Decimal priceBegin { get; set; }

        [DataType(DataType.Date)]
        public DateTime actualDate { get; set; }

        public string fullDescription { get; set; }
        
    }
}