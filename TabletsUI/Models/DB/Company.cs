﻿using System.ComponentModel.DataAnnotations;

namespace TabletsUI.Models.DB
{ 
    public class Company
    {
        [Key]
        public int id { get; set; }
        public string name { get; set; }

        public Company() : this("Unknown") { }
        public Company(string companyName)
        {
            name = companyName;
        }
    }
}
