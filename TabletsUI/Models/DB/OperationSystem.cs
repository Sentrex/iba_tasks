﻿
using System.ComponentModel.DataAnnotations;

namespace TabletsUI.Models.DB
{
    public class OperationSystem
    {
        [Key]
        public int id { get; set; }
        public string name { get; set; }

        public OperationSystem() : this("Unknown") { }
        public OperationSystem(string name)
        {
            this.name = name;
        }
    }
}
