﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using TabletsUI.Models.DB;

namespace TabletsUI.Models
{
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        //main table
        public DbSet<Tablet> Tablets { get; set; }


        //support
        public DbSet<OperationSystem> Operation_systems { get; set; }
        public DbSet<Definition> Definitions { get; set; }
        public DbSet<Color> Colors { get; set; }
        public DbSet<Company> Companies { get; set; }

        //log
        public DbSet<Error> error_log { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //mapping to OS table
            modelBuilder.Entity<OperationSystem>().ToTable("Operation_systems");
            //mapping to Definitions table
            modelBuilder.Entity<Definition>().ToTable("Definitions");
            //mapping to Colors table
            modelBuilder.Entity<Color>().ToTable("Colors");
            //mapping to Companies table
            modelBuilder.Entity<Company>().ToTable("Companies");
            
            base.OnModelCreating(modelBuilder);
        }


    }
}