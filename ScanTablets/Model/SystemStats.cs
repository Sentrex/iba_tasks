﻿using OpenQA.Selenium;
using System.Collections.Generic;

namespace ScanTablets.Model
{
    public static class SystemStats
    {
        public static int PagesCount = 1;

        public static int CurrentPage=1;
        public static List<Tablet> Tablets = new List<Tablet>();

        /// <summary>
        /// Adding a table to the list of tablets
        /// </summary>
        /// <param name="tablet">A new tablet</param>
        public static void AddTablet(Tablet tablet)
        {
            Tablets.Add(tablet);
        }
    }
}
