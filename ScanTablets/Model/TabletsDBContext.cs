﻿using System.Data.Entity;

namespace ScanTablets
{
    public class TabletsDBContext : DbContext
    {
        public TabletsDBContext() : base("DBconnection") { }
        
        //main table
        public DbSet<Tablet> Tablets { get; set; }

        //support
        public DbSet<OperationSystem> Operation_systems { get; set; }
        public DbSet<Definition> Definitions { get; set; }
        public DbSet<Color> Colors { get; set; }
        public DbSet<Company> Companies { get; set; }

        //log
        public DbSet<Error> error_log { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //mapping to OS table
            modelBuilder.Entity<OperationSystem>().ToTable("Operation_systems");
            //mapping to Definitions table
            modelBuilder.Entity<Definition>().ToTable("Definitions");
            //mapping to Colors table
            modelBuilder.Entity<Color>().ToTable("Colors");
            //mapping to Companies table
            modelBuilder.Entity<Company>().ToTable("Companies");

            base.OnModelCreating(modelBuilder);
        }

    }
}
