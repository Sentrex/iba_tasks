﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using ScanTables;
using ScanTablets.Model;
using System;
using System.Collections.Generic;
using System.Threading;

namespace ScanTablets
{
    public partial class StartClass
    {
        #region Reading
        /// <summary>
        /// Read tablets' names
        /// </summary>
        private void ReadTablets()
        {
            IList<IWebElement> tabletsNames = null;
            IList<IWebElement> info = null;
            IList<IWebElement> prices = null;

            tabletsNames = driver.FindElements(By.XPath("/ html[@class = 'non-responsive-layout'] / body[@class = 'no-touch'] / div[@id = 'container'] / div[@class = 'g-container-outer'] / div[@class = 'l-gradient-wrapper'] / div[@class = 'g-middle'] / div[@class = 'g-middle-i'] / div[@class = 'catalog-content js-scrolling-area'] / div[@class = 'schema-grid__wrapper'] / div[@class = 'schema-grid'] / div[@class = 'js-schema-results schema-grid__center-column'] / div[@id = 'schema-products']//div[@class='schema-product__group']/div[@class='schema-product']/div[@class='schema-product__part schema-product__part_2']/div[@class='schema-product__part schema-product__part_4']/div[@class='schema-product__title']/a/span"));
            info = driver.FindElements(By.XPath("/html[@class='non-responsive-layout']/body[@class='no-touch']/div[@id='container']/div[@class='g-container-outer']/div[@class='l-gradient-wrapper']/div[@class='g-middle']/div[@class='g-middle-i']/div[@class='catalog-content js-scrolling-area']/div[@class='schema-grid__wrapper']/div[@class='schema-grid']/div[@class='js-schema-results schema-grid__center-column']/div[@id='schema-products']/div[@class='schema-product__group']/div[@class='schema-product']/div[@class='schema-product__part schema-product__part_2']/div[@class='schema-product__part schema-product__part_4']/div[@class='schema-product__description']/span"));
            prices = driver.FindElements(By.XPath("/html[@class='non-responsive-layout']/body[@class='no-touch']/div[@id='container']/div[@class='g-container-outer']/div[@class='l-gradient-wrapper']/div[@class='g-middle']/div[@class='g-middle-i']/div[@class='catalog-content js-scrolling-area']/div[@class='schema-grid__wrapper']/div[@class='schema-grid']/div[@class='js-schema-results schema-grid__center-column']/div[@id='schema-products']/div[@class='schema-product__group']/div[@class='schema-product']/div[@class='schema-product__part schema-product__part_2']/div[@class='schema-product__part schema-product__part_3']/div[@class='schema-product__price-group']/div[@class='schema-product__line'][1]/div[@class='schema-product__price']/a[@class='schema-product__price-value schema-product__price-value_primary']/span"));

            if (tabletsNames != null
                && tabletsNames.Count > 0 
                && tabletsNames.Count == info.Count 
                && info.Count == prices.Count)
            {
                for (int i= 0; i < tabletsNames.Count;i++)
                {
                    SystemStats.AddTablet(Parser.ParseFromPage(info: info[i], title: tabletsNames[i], price: prices[i]));
                }
            }
            else
            {
                Console.WriteLine("\nAn error of reading occured on page {0}", SystemStats.CurrentPage);
                Console.ReadLine();
            }
            RefreshState();
        }

        /// <summary>
        /// Gets pages count
        /// </summary>
        private void ReadPageCount(int flagNewTablets)
        {
            //click on the button for get list of pages
            //reading list of pages
            IList<IWebElement> Pages;
            if (flagNewTablets == 1)
            {
                SetMinPrice();
                //click on the button for get list of pages
                driver.FindElement(By.XPath("/html[@class='non-responsive-layout']/body[@class='no-touch']/div[@id='container']/div[@class='g-container-outer']/div[@class='l-gradient-wrapper']/div[@class='g-middle']/div[@class='g-middle-i']/div[@class='catalog-content js-scrolling-area']/div[@class='schema-grid__wrapper']/div[@class='schema-grid']/div[@class='js-schema-results schema-grid__center-column']/div[@id='schema-pagination']/div[@class='schema-pagination__secondary']/div[@class='schema-pagination__dropdown']")).Click();
                Pages = driver.FindElements(By.XPath("/html[@class='non-responsive-layout']/body[@class='no-touch']/div[@id='container']/div[@class='g-container-outer']/div[@class='l-gradient-wrapper']/div[@class='g-middle']/div[@class='g-middle-i']/div[@class='catalog-content js-scrolling-area']/div[@class='schema-grid__wrapper']/div[@class='schema-grid']/div[@class='js-schema-results schema-grid__center-column']/div[@id='schema-pagination']/div[@class='schema-pagination__pages schema-pagination__pages_active']/div[@class='schema-pagination__pages-container mCustomScrollbar _mCS_1 mCS_no_scrollbar']/div[@id='mCSB_1']/div[@id='mCSB_1_container']/ul[@class='schema-pagination__pages-list']/li[@class='schema-pagination__pages-item']//a[@class='schema-pagination__pages-link']"));
            }
            else
            {
                //click on the button for get list of pages
                driver.FindElement(By.XPath("/html[@class='non-responsive-layout']/body[@class='no-touch']/div[@id='container']/div[@class='g-container-outer']/div[@class='l-gradient-wrapper']/div[@class='g-middle']/div[@class='g-middle-i']/div[@class='catalog-content js-scrolling-area']/div[@class='schema-grid__wrapper']/div[@class='schema-grid']/div[@class='js-schema-results schema-grid__center-column']/div[@id='schema-pagination']/div[@class='schema-pagination__secondary']/div[@class='schema-pagination__dropdown']")).Click();
                Pages = driver.FindElements(By.XPath("/html[@class='non-responsive-layout']/body[@class='no-touch']/div[@id='container']/div[@class='g-container-outer']/div[@class='l-gradient-wrapper']/div[@class='g-middle']/div[@class='g-middle-i']/div[@class='catalog-content js-scrolling-area']/div[@class='schema-grid__wrapper']/div[@class='schema-grid']/div[@class='js-schema-results schema-grid__center-column']/div[@id='schema-pagination']/div[@class='schema-pagination__pages schema-pagination__pages_active']/div[@class='schema-pagination__pages-container mCustomScrollbar _mCS_1']/div[@id='mCSB_1']/div[@id='mCSB_1_container']/ul[@class='schema-pagination__pages-list']/li[@class='schema-pagination__pages-item']//a[@class='schema-pagination__pages-link']"));
            }
            //setting maxnumber of pages
            SystemStats.PagesCount = Pages.Count + 1;
            var btn4hideHScroll = driver.FindElement(By.XPath("/html[@class='non-responsive-layout']/body[@class='no-touch']/div[@id='container']/div[@class='g-container-outer']/div[@class='l-gradient-wrapper']/div[@class='g-middle']/div[@class='g-middle-i']/div[@class='catalog-content js-scrolling-area']/div[@class='schema-grid__wrapper']/div[@class='schema-grid']/div[@class='js-schema-results schema-grid__center-column']/div[@id='schema-products']/div[@class='schema-product__group'][29]/div[@class='schema-product']/div[@class='schema-product__part schema-product__part_2']"));
            btn4hideHScroll.Click();
        }

        private void GoNextPage()
        {
            if (SystemStats.CurrentPage >= SystemStats.PagesCount)
            {
                return;
            }
            else
            {
                SystemStats.CurrentPage++;
            }
            var nextButton = driver.FindElement(By.XPath("/html[@class='non-responsive-layout']/body[@class='no-touch']/div[@id='container']/div[@class='g-container-outer']/div[@class='l-gradient-wrapper']/div[@class='g-middle']/div[@class='g-middle-i']/div[@class='catalog-content js-scrolling-area']/div[@class='schema-grid__wrapper']/div[@class='schema-grid']/div[@class='js-schema-results schema-grid__center-column']/div[@id='schema-pagination']/a[@class='schema-pagination__main']"));
            Actions actions = new Actions(driver);
            actions.MoveToElement(nextButton);
            actions.Perform();
            if (nextButton != null)
                nextButton.Click();
        }

        #endregion

        /// <summary>
        /// setter for dislpaying actual tablets
        /// </summary>
        private void SetMinPrice()
        {
            driver.FindElement(By.XPath("/html[@class='non-responsive-layout']/body[@class='no-touch']/div[@id='container']/div[@class='g-container-outer']/div[@class='l-gradient-wrapper']/div[@class='g-middle']/div[@class='g-middle-i']/div[@class='catalog-content js-scrolling-area']/div[@class='schema-grid__wrapper']/div[@class='schema-grid']/div[@class='schema-grid__left-column']/div[@class='schema-filter__wrapper']/div[@id='schema-filter']/div[1]/div[@class='schema-filter__fieldset'][2]/div[@class='schema-filter__facet']/div[@class='schema-filter__group']/div[@class='schema-filter-control schema-filter-control_input'][1]/input[@class='schema-filter-control__item schema-filter__number-input schema-filter__number-input_price']")).SendKeys("1");
            driver.FindElement(By.XPath("/html[@class='non-responsive-layout']/body[@class='no-touch']/div[@id='container']/div[@class='g-container-outer']/div[@class='l-gradient-wrapper']/div[@class='g-middle']/div[@class='g-middle-i']/div[@class='catalog-content js-scrolling-area']/div[@class='schema-grid__wrapper']/div[@class='schema-header']/div[@id='schema-segments']/div[@class='schema-filter__group']/label[@class='schema-filter-control schema-filter-control_switcher'][2]/span[@class='schema-filter-control__switcher-inner']")).Click();
            Thread.Sleep(Constants.THREAD_SLEEP_DURATION_SETTER);
        }
    }
}
