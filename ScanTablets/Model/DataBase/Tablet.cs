﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ScanTablets
{
    public class Tablet
    {
        [Key]
        public int id { get; set; }
        public int manufacturer { get; set; }
        public string name { get; set; }
        public int OS { get; set; }
        public int flash { get; set; }
        public int color { get; set; }
        public int definition { get; set; }
        public Decimal priceBegin { get; set; }
        public DateTime actualDate { get; set; }
        public string fullDescription { get; set; }

        public Tablet() : this(0,"",0,0,0,0,0,""){ }
        public Tablet(int manufacturer, string name, int OS, int flash, int color, int definition, Decimal price, string description)
        {
            this.manufacturer = manufacturer;
            this.name = name;
            this.OS = OS;
            this.flash = flash;
            this.color = color;
            this.definition = definition;
            this.priceBegin = price;
            this.actualDate = DateTime.Now;
            fullDescription = description;
        }
    }
}