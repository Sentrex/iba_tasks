﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ScanTablets
{
    public class Error
    {
        [Key]
        public int id { get; set; }
        public string text { get; set; }
        public DateTime date { get; set; }

        public Error() : this("", DateTime.Today) {}
        public Error(string message, DateTime date)
        {
            this.date = date;
            text = message;
        }
    }
}