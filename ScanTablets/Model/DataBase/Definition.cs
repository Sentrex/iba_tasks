﻿using System.ComponentModel.DataAnnotations;

namespace ScanTablets
{
    public class Definition
    {
        [Key]
        public int id { get; set; }
        public string definition { get; set; }

        public Definition() : this("Unknown") { }
        public Definition(string definition)
        {
            this.definition = definition;
        }
    }
}
