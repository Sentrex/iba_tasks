﻿using System.ComponentModel.DataAnnotations;

namespace ScanTablets
{
    public class Color
    {
        [Key]
        public int id { get; set; }
        public string color { get; set; }

        public Color() : this("Unknown") { }
        public Color(string colorName)
        {
            color = colorName;
        }
    }
}
