﻿using OpenQA.Selenium;
using System;
using System.Data.Linq;
using System.Linq;

namespace ScanTablets
{
    public static class Parser
    {
        public static Tablet ParseFromPage(IWebElement title, IWebElement info, IWebElement price)
        {
            //An object of a new tablet
            Tablet newTablet;
            //fields of object for initialization
            int manufacturer = -1;
            string name;
            int OS = -1;
            int flash = 0;
            int color = -1;
            int definition = -1;
            Decimal priceBegin;
            string description = info.Text;

            //parsing to fields
            name = title.Text;
            priceBegin = GetPrice(price.Text);
            ParseInfo(operationSystem: ref OS, flashMemory: ref flash, color: ref color, definition: ref definition, manufacturer: ref manufacturer, infoString: info.Text, titleString: title.Text);
            newTablet = new Tablet(manufacturer, name, OS, flash, color, definition, priceBegin, description);

            return newTablet;
        }

        /// <summary>
        /// Get parsed price
        /// </summary>
        /// <param name="priceString">String with price value from page</param>
        /// <returns>Parsed price value</returns>
        private static Decimal GetPrice(string priceString)
        {
            string priceValue = priceString.Substring(0, priceString.Length - 3);
            return Decimal.Parse(priceValue);
        }

        /// <summary>
        /// Parsing and init from info
        /// </summary>
        /// <param name="operationSystem">operation system id</param>
        /// <param name="flashMemory">flash memory</param>
        /// <param name="color">color id</param>
        /// <param name="definition">definition id</param>
        /// <param name="infoString">String with no-parsed values</param>
        private static void ParseInfo(ref int operationSystem, ref int flashMemory, ref int color, ref int definition, string infoString, ref int manufacturer, string titleString)
        {
            using (TabletsDBContext DBContext = new TabletsDBContext())
            {
                string manufacturer_name;
                if (infoString.Split(',').Length < 3)
                {
                    operationSystem = DBContext.Operation_systems.Where(OS => OS.name == "Unknown").Select(OS => OS.id).First();
                    definition = DBContext.Definitions.Where(definition_search => definition_search.definition == "Unknown").Select(definition_search => definition_search.id).First();
                    color = DBContext.Colors.Where(color_search => color_search.color == "Unknown").Select(color_search => color_search.id).First();
                    manufacturer_name = titleString.Split(' ')[0];
                    manufacturer = DBContext.Companies.Where(company_search => company_search.name == "" + manufacturer_name + "").Select(company_search => company_search.id).First();
                    return;
                }
                string[] str_values = infoString.Split(',');
                string OS_str_value = str_values[1].Trim();
                string definition_str_value = DefinitionParse(str_values[0]);
                string color_str_value = ColorParse(str_values[str_values.Length - 1]);
                foreach (string splitted_value in str_values)
                {
                    if (splitted_value.Contains("флэш-память"))
                    {
                        flashMemory = Int32.Parse(splitted_value.Split(' ')[2]);
                        break;
                    }
                    else
                        continue;
                }

                try
                {
                    //getting OS index
                    operationSystem = DBContext.Operation_systems.Where(OS => OS.name == "" + OS_str_value + "").Select(OS => OS.id).First();
                }
                catch (InvalidOperationException)
                {
                    OS_str_value = "Unknown";
                    operationSystem = DBContext.Operation_systems.Where(OS => OS.name == "" + OS_str_value + "").Select(OS => OS.id).First();
                }

                try
                {
                    //getting definition index
                    definition = DBContext.Definitions.Where(definition_search => definition_search.definition == "" + definition_str_value + "").Select(definition_search => definition_search.id).First();
                }
                catch (InvalidOperationException)
                {
                    definition = DBContext.Definitions.Where(definition_search => definition_search.definition == "Unknown").Select(definition_search => definition_search.id).First();
                }

                try
                {
                    //getting color index
                    color = DBContext.Colors.Where(color_search => color_search.color == "" + color_str_value + "").Select(color_search => color_search.id).First();
                }
                catch (InvalidOperationException)
                {
                    color_str_value = "Unknown";
                    color = DBContext.Colors.Where(color_search => color_search.color == "" + color_str_value + "").Select(color_search => color_search.id).First();
                }

                try
                {
                    //getting manufacturer index
                    manufacturer_name = titleString.Split(' ')[0];
                    manufacturer = DBContext.Companies.Where(company_search => company_search.name == "" + manufacturer_name + "").Select(company_search => company_search.id).First();
                }
                catch (InvalidOperationException)
                {
                    manufacturer_name = "Unknown";
                    manufacturer = DBContext.Companies.Where(company_search => company_search.name == "" + manufacturer_name + "").Select(company_search => company_search.id).First();
                }
            }
        }

        /// <summary>
        /// Parsing for get a definition
        /// </summary>
        /// <param name="inputString">no-parse string with definition</param>
        /// <returns>parsed definition</returns>
        private static string DefinitionParse(string inputString)
        {
            string definition = inputString.Split(' ')[inputString.Split(' ').Length - 1];
            return definition.Substring(1, definition.Length - 2);
        }

        /// <summary>
        /// Parsing for get an color
        /// </summary>
        /// <param name="inputString">no-parse string with color</param>
        /// <returns>parsed color name</returns>
        private static string ColorParse(string inputString)
        {
            string color = inputString.Split(' ')[inputString.Split(' ').Length - 1];
            if (color.Contains("/"))
            {
                return color.Split('/')[color.Split('/').Length - 1];
            }
            else
                return color;
        }

    }
}
