﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using ScanTables;
using ScanTablets.Model;
using System;
using System.Linq;
using System.Threading;

namespace ScanTablets
{
    public partial class StartClass
    {
        ChromeDriver driver;
        TabletsDBContext DBContext;

        public StartClass() : this(driver: null, DBContext: null) { }
        public StartClass(ChromeDriver driver, TabletsDBContext DBContext)
        {
            this.DBContext = DBContext;       //opening the connection to the DBContext
            this.driver = driver;
        }

        /// <summary>
        /// Start the app
        /// </summary>
        public int Start()
        {
            Console.Clear();
            Console.WriteLine("Press some button for start scan...");
            Console.ReadKey();
            Console.Clear();
            Console.WriteLine("The app is starting...");
            try
            {
                DBContext = new TabletsDBContext();
                driver = new ChromeDriver(@"d:\Work\DLL\Chrome Driver\", commandTimeout: TimeSpan.FromSeconds(10), options: new ChromeOptions());
                Console.WriteLine("Opening the website...");
                driver.Navigate().GoToUrl("http://www.catalog.onliner.by/tabletpc");        //open the website
                ReadPageCount(ChooseMode());
                for (int i = 1; i < SystemStats.PagesCount + 1; i++)
                {
                    Thread.Sleep(Constants.THREAD_SLEEP_DURATION);
                    ReadTablets();
                    GoNextPage();
                }
                driver.Close();
                SynchronizeWithDB();
                return CheckExit();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                return PrintError(ex.Message, ex);
            }
            catch (ArgumentException ex)
            {
                return PrintError(ex.Message, ex);
            }
            catch (FormatException ex)
            {
                return PrintError(ex.Message, ex);
            }
            catch (OverflowException ex)
            {
                return PrintError(ex.Message, ex);
            }
            catch (NotFoundException ex)
            {
                return PrintError("Element or file not found. The app will be closed.", ex);
            }
            catch (DriverServiceNotFoundException ex)
            {
                return PrintError("File of driver service not found. The app will be closed.", ex);
            }
            catch (WebDriverTimeoutException ex)
            {
                return PrintError("Timeout. The app will be closed.", ex);
            }
            catch (WebDriverException ex)
            {
                return PrintError("WebBrowser or driver was unexpected closed. The app will be closed.", ex);
            }
            catch (Exception ex)
            {
                return PrintError("An some error occurred. The app will be closed.", ex);
            }
        }

        #region Others methods
        /// <summary>
        /// Close the app
        /// </summary>
        private void Exit()
        {
            if (driver != null)
            {
                driver.Quit();
            }
            Environment.Exit(0);
        }

        /// <summary>
        /// Update the execution state of the program
        /// </summary>
        private void RefreshState()
        {
            Console.Clear();
            var progress = (double) SystemStats.CurrentPage / SystemStats.PagesCount;
            Console.WriteLine("Progress: {0}", progress.ToString("#0.00%"));
            Console.WriteLine("Completed page {0} of {1}", SystemStats.CurrentPage, SystemStats.PagesCount);
            Console.WriteLine("Tablets count: {0}", SystemStats.Tablets.Count);
        }

        /// <summary>
        /// Print error
        /// </summary>
        /// <param name="message">Message for print</param>
        /// <param name="ex">Exception</param>
        private int PrintError(string message, Exception ex)
        {
            Console.WriteLine("\n{0}", message);
            Error error1 = new Error(ex.Message, DateTime.Now);
            DBContext.error_log.Add(error1);
            DBContext.SaveChanges();
            Console.ReadKey();
            Exit();
            return -1;
        }

        /// <summary>
        /// Waiting 'ok' for exit
        /// </summary>
        private int CheckExit()
        {
            Console.Clear();
            Console.WriteLine("********************************");
            Console.WriteLine("Pages was readed in total: {0} of {1}", SystemStats.CurrentPage, SystemStats.PagesCount);
            Console.WriteLine("Tablets count: {0}", SystemStats.Tablets.Count);
            Console.Write("********************************\nThe app finished.\nPrint 'OK' for close or 'r' for restart: ");
            string exitKey = Console.ReadLine();
            switch (exitKey)
            {
                case "OK":
                case "ok":
                    Exit();
                    return 0;
                case "r":
                case "R":
                    return 1;
                default:
                    return CheckExit();
            }
        }

        /// <summary>
        /// Setting mode
        /// </summary>
        /// <returns>Mod's flag</returns>
        private int ChooseMode()
        {
            Console.Clear();
            Console.Write("Do you wanna read all tablets from website or actual? \nFor all print '1', for actual '2': ");
            string mode = Console.ReadLine();
            if (mode.Equals("1"))
            {
                return 0;
            }
            else if (mode.Equals("2"))
            {
                return 1;
            }
            else
            {
                return ChooseMode();
            }
        }
        #endregion

        /// <summary>
        /// Synchronize data with database
        /// </summary>
        private void SynchronizeWithDB()
        {
            foreach(Tablet tablet in SystemStats.Tablets)
            {
                DBContext.Tablets.Add(tablet);
                DBContext.SaveChanges();
            }
        }
        
    }
}
