﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScanTables
{
    public static class Constants
    {
        //waiting for cycle
        public const int THREAD_SLEEP_DURATION = 1500;
        //waiting for the price setter
        public const int THREAD_SLEEP_DURATION_SETTER = 1000;
    }
}
